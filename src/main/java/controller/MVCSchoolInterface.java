package controller;

        public interface MVCSchoolInterface {

    interface SchoolController{

        void createNewStudent();
        void showStudentPool();

    }

    interface GradeBookController{
        void createNewStudentsGrade(long subjectId, long studentId, long gradeId);
    }

}
