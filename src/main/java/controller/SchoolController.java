package controller;

import View.PrivateSchoolView;
import View.PrivateSchoolViewImpl;
import model.*;

public class SchoolController implements MVCSchoolInterface.SchoolController{

    private StudentPool studentPool = new StudentPool();
    private StudentSubjectPair studentSubjectPair = new StudentSubjectPair();
    private StudentToSubjectToGrade studentToSubjectToGrade = new StudentToSubjectToGrade();
    private StudentSubjectAndGradePair studentSubjectAndGradePair;
    private StudentToSubject studentToSubject= new StudentToSubject();
    private PrivateSchoolView view = new PrivateSchoolViewImpl();


    public void createNewStudent(){
        String name = view.askForStudentName();
        String surname = view.askForStudentSurname();
        Student student = new Student(name, surname);
        studentPool.addNewStudent(student);
    }

    public void showStudentPool(){
        System.out.println(studentPool.getStudentList().toString());
    }






    public StudentSubjectPair addStudentAndSubjectPair(StudentSubjectPair studentSubjectPair){
        studentToSubject.getStudentSubjectPairsList().add(studentSubjectPair);
        return studentSubjectPair;
    }

    public void addGrade(StudentSubjectAndGradePair studentSubjectAndGradePair){
        studentToSubjectToGrade.getStudentSubjectAndGradePairsList().add(studentSubjectAndGradePair);
    }


}

