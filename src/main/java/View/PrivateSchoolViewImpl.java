package View;

public class PrivateSchoolViewImpl implements PrivateSchoolView {

    PrinterWrapper printerWrapper = new PrinterWrapperImpl();
    ScannerWrapper scannerWrapper = new ScannerWrapperImpl();



    public String askForStudentName(){
        printerWrapper.print("Enter name of student:");
        String name = scannerWrapper.userDataEnter();
        return name;
    }

    public String askForStudentSurname(){
        printerWrapper.print("Enter surname of student:");
        String surname = scannerWrapper.userDataEnter();
        return surname;
    }

    public void communicateOperationIsSuccesfull(){
        printerWrapper.print("Mission completed.");
    }
}
