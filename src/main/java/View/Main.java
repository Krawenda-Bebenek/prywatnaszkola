package View;

import controller.SchoolController;

public class Main {
    public static void main(String[] args) {
        SchoolController controller = new SchoolController();
        PrinterWrapper printerWrapper = new PrinterWrapperImpl();

        controller.createNewStudent();
        controller.showStudentPool();
    }
}
