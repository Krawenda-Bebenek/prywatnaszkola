package View;

public interface PrinterWrapper {

    void print(String text);
}
