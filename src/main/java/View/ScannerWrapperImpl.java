package View;

import java.util.Scanner;

public class ScannerWrapperImpl implements ScannerWrapper{

    private Scanner scanner = new Scanner(System.in);

    public String userDataEnter(){
        return scanner.nextLine();
    }
}
