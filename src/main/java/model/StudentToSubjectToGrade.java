package model;

import model.StudentSubjectAndGradePair;
import model.StudentSubjectPair;

import java.util.LinkedList;
import java.util.List;

public class StudentToSubjectToGrade {

    private List<StudentSubjectAndGradePair> studentSubjectAndGradePairsList = new LinkedList<>();

    public List<StudentSubjectAndGradePair> getStudentSubjectAndGradePairsList() {
        return studentSubjectAndGradePairsList;
    }

    public void setStudentSubjectAndGradePairsList(List<StudentSubjectAndGradePair> studentSubjectAndGradePairsList) {
        this.studentSubjectAndGradePairsList = studentSubjectAndGradePairsList;
    }

    public void  createStudentSubjectAndGradePair(StudentSubjectPair studentSubjectPair, long gradeId){
        long studentSubjectAndGradePairId = studentSubjectAndGradePairsList.get(studentSubjectAndGradePairsList.size()-1).getStudentToSubjectPairId()+1;
        StudentSubjectAndGradePair studentSubjectAndGradePair = new StudentSubjectAndGradePair(studentSubjectAndGradePairId,
                studentSubjectPair.getPairId(), gradeId);
        studentSubjectAndGradePairsList.add(studentSubjectAndGradePair);

    }
}
