package model;

import java.util.LinkedList;
import java.util.List;

public class SubjectPool {

    private List<Subject> pool = new LinkedList<>();

    private SubjectPool(){
        pool.add(new Subject(1l, "Math"));
        pool.add(new Subject(2l, "English"));
        pool.add(new Subject(3l, "Chemistry"));
        pool.add(new Subject(4l, "Biology"));
        pool.add(new Subject(5l, "Physic"));
        pool.add(new Subject(6l, "History"));
        pool.add(new Subject(7l, "P.E"));
        pool.add(new Subject(8l, "Religion"));
        pool.add(new Subject(9l, "Spanish"));
    }

    public List<Subject> getPool() {
        return pool;
    }

    public void setPool(List<Subject> pool) {
        this.pool = pool;
    }

    public void displaySubjectList(){
        for(Subject subject : pool){
            System.out.println(subject);
        }
    }
}
