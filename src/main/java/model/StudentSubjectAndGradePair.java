package model;

public class StudentSubjectAndGradePair {

    private long pairId;
    private long studentToSubjectPairId;
    private long gradeId;

    public long getPairId() {
        return pairId;
    }

    public void setPairId(long pairId) {
        this.pairId = pairId;
    }

    public long getStudentToSubjectPairId() {
        return studentToSubjectPairId;
    }

    public void setStudentToSubjectPairId(long studentToSubjectPairId) {
        this.studentToSubjectPairId = studentToSubjectPairId;
    }

    public long getGradeId() {
        return gradeId;
    }

    public void setGradeId(long gradeId) {
        this.gradeId = gradeId;
    }

    public StudentSubjectAndGradePair(long pairId, long studentToSubjectId, long gradeId) {
        this.pairId = pairId;
        this.studentToSubjectPairId = studentToSubjectPairId;
        this.gradeId = gradeId;
    }
}
