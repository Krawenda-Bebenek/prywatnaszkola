package model;

import model.StudentSubjectPair;

import java.util.LinkedList;
import java.util.List;

public class StudentToSubject {

    private List<StudentSubjectPair> studentSubjectPairsList = new LinkedList<>();

    public List<StudentSubjectPair> getStudentSubjectPairsList() {
        return studentSubjectPairsList;
    }

    public void setStudentSubjectPairsList(List<StudentSubjectPair> studentSubjectPairsList) {
        this.studentSubjectPairsList = studentSubjectPairsList;
    }

    public void createStudentToSubject(long studentId, long subjectId){
        long studentSubjectPairId = (studentSubjectPairsList.get(studentSubjectPairsList.size()-1)).getPairId() +1;
        StudentSubjectPair studentSubjectPair = new StudentSubjectPair(studentSubjectPairId,studentId,subjectId);
        studentSubjectPairsList.add(studentSubjectPair);
    }
}
