package model;

import java.util.ArrayList;
import java.util.List;

public class StudentPool {

    private List<Student> studentList = new ArrayList<>();

    public Student addNewStudent(Student student){
        long id;
        if (studentList.size() == 0) {
            id = 1;
        } else {
            id = (studentList.get(studentList.size() - 1)).getId() + 1;
        }
        student.setId(id);
        studentList.add(student);
        return student;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }
}
