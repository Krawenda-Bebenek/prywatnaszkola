package model;

public class StudentSubjectPair {

    private long pairId;
    private long studentId;
    private long subjectId;

    public StudentSubjectPair(long pairId, long studentId, long subjectId) {
        this.pairId = pairId;
        this.studentId = studentId;
        this.subjectId = subjectId;
    }

    public long getPairId() {
        return pairId;
    }

    public void setPairId(long pairId) {
        this.pairId = pairId;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public StudentSubjectPair() {
    }
}
